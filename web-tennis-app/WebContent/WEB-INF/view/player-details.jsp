<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>
<head>
	<title>Player details</title>
	
	<link type="text/css" 
	rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />
	
	<link type="text/css" 
	rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/add-customer-style.css" />
</head>

<body>
	<div id="wrapper">
		<div id="header">
			<h2>Player details</h2>
		</div>
	</div>
	
	<div id="container">
		<img src="${player.profileImageUrl}" height="100" width="100"><br>
	
		First name:
		${player.firstName} <br>
		
		Last name:
		${player.lastName} <br>
		
		Description:
		${player.description} <br>
		
		Points:
		${player.points} <br>
		
		Birth date:
		${player.dateOfBirth}"<br>
		
		Is player professional:
		<c:choose>
			<c:when test="${player.isProffesional=='1'}">yes</c:when>
			<c:when test="${player.isProffesional=='0'}">no</c:when>
		</c:choose>
		
		
		
		
	</div>
</body>
</html>