<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>

<html>
<head>
	<title>Save player</title>
	
	<link type="text/css" 
	rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/style.css" />
	
	<link type="text/css" 
	rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/add-customer-style.css" />
</head>

<body>
	<div id="wrapper">
		<div id="header">
			<h2>Player manager</h2>
		</div>
	</div>
	
	<div id="container">
		<h3>Save player</h3>
		
		<form:form action="savePlayer" modelAttribute="player" method="POST">
			
			<form:hidden path="id"/>
		
			<label>First name:</label>
			<form:input path="firstName"/><br>
			
			<label>Last name:</label>
			<form:input path="lastName"/><br>
						
			<label>Description:</label>
			<form:input path="description"/><br>
			
			<label>Points:</label>
			<form:input path="points"/><br>
			
			 <label>Professional:</label>
			<form:select path="isProffesional">
				<form:option value="0" selected="selected">Not professional</form:option>
				<form:option value="1">Professional</form:option>
			</form:select><br>
			
			<label>Profile Image URL:</label>
			<form:input path="profileImageUrl"/><br>
			
			
			<input type="submit" value="Save" class="save" />
			
		</form:form>
	</div>
</body>
</html>