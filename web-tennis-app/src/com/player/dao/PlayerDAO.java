package com.player.dao;

import java.util.List;

import com.player.entity.Player;

public interface PlayerDAO {

	public List<Player> getPlayers();

	public void savePlayer(Player thePlayer);

	public Player getPlayer(int theId);

	public Object deletePlayer(int theId);
}
