package com.player.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="player")
public class Player {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)	
	@Column(name="id")
	private int id;
	
	@Column(name="first_name")
	private String firstName;
	
	@Column(name="last_name")
	private String lastName;
	
	@Column(name="description")
	private String description;
	
	@Column(name="points")
	private int points;
	
	@DateTimeFormat(pattern="YYYY-MM-DD")
	@Column(name="date_of_birth")
	private Date dateOfBirth;
	
	@Column(name="is_proffesional")
	private byte isProffesional;
	
	@Column(name="profile_image_url")
	private String profileImageUrl;

	public Player() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getProfileImageUrl() {
		return profileImageUrl;
	}

	public void setProfileImageUrl(String profileImageUrl) {
		this.profileImageUrl = profileImageUrl;
	}

	public byte getIsProffesional() {
		return isProffesional;
	}

	public void setIsProffesional(byte isProffesional) {
		this.isProffesional = isProffesional;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", description="
				+ description + ", points=" + points + ", dateOfBirth=" + dateOfBirth + ", isProffesional="
				+ isProffesional + ", profileImageUrl=" + profileImageUrl + "]";
	}

	
	
	}

	
	
	

	
	
	
	
	

