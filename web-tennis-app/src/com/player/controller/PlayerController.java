package com.player.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.player.entity.Player;
import com.player.service.PlayerService;

@Controller
@RequestMapping("/player")
public class PlayerController {

	@Autowired
	private PlayerService playerService;
	
	@GetMapping("/list")
	public String listPlayers(Model theModel) {
		
		List<Player> thePlayers = playerService.getPlayers();
		
		theModel.addAttribute("players", thePlayers);
		
		return "list-players";
	}
	
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {
		Player thePlayer = new Player();
		
		theModel.addAttribute("player", thePlayer);
		
		return "player-form";
	}
	
	@PostMapping("/savePlayer")
	public String savePlayer(@ModelAttribute("player") Player thePlayer) {
		
		playerService.savePlayer(thePlayer);
		
		return "redirect:/player/list";
	}
	
	@GetMapping("/showFormForUpdate")
	public String showFormForUpdate(@RequestParam("playerId") int theId,
									Model theModel) {
		
		Player thePlayer = playerService.getPlayer(theId);
		
		theModel.addAttribute("player", thePlayer);
		
		return "player-form";
		
	}
	
	@GetMapping("/showDetailsPage")
	public String showFormForDetail(@RequestParam("playerId") int theId,
									Model theModel) {
		
		Player thePlayer = playerService.getPlayer(theId);
		
		theModel.addAttribute("player", thePlayer);
		
		return "player-details";
		
	}
	
	@GetMapping("/delete")
	public String deletePlayer(@RequestParam("playerId") int theId,
									Model theModel) {
		
		playerService.deletePlayer(theId);
		
		return "redirect:/player/list";
		
	}
	
		
	
}
