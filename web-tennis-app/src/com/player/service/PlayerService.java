package com.player.service;

import java.util.List;

import com.player.entity.Player;

public interface PlayerService {

	public List<Player> getPlayers();

	public void savePlayer(Player thePlayer);

	public Player getPlayer(int theId);

	public void deletePlayer(int theId);
}
